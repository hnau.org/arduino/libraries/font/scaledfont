#ifndef SCALEDFONT_H
#define SCALEDFONT_H

#include <Font.h>

class ScaledFont: public Font {
	
private:

	Font& font;
	uint8_t scale;
	

public:

	ScaledFont(
		Font& font_,
		uint8_t scale
	);
	
	
	virtual size_t getGlyphWidth() override;
	virtual size_t getGlyphHeight() override;
	
	virtual bool getPixel(Glyph glyph, size_t x, size_t y) override;
	virtual bool checkGlyphIsAvailable(Glyph glyph) override;

};


#endif //SCALEDFONT_H
