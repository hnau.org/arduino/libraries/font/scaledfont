#include "ScaledFont.h"

ScaledFont::ScaledFont(
		Font& font_,
		uint8_t scale
): font(font_) {
	this->scale = scale;
}


size_t ScaledFont::getGlyphWidth() {
	return font.getGlyphWidth() * scale;
}

size_t ScaledFont::getGlyphHeight() {
	return font.getGlyphHeight() * scale;
}
	
bool ScaledFont::getPixel(Glyph glyph, size_t x, size_t y) {
	return font.getPixel(glyph, x / scale, y / scale);
}

bool ScaledFont::checkGlyphIsAvailable(Glyph glyph) {
	return font.checkGlyphIsAvailable(glyph);
}
